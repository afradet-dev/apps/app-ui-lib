/*
 * Public API Surface of ngx-afr-ui
 */

export * from './lib/ngx-afr-ui.service';
export * from './lib/ngx-afr-ui.component';
